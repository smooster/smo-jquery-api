$(document).ready ->
    page_counter = 0

    template = Hogan.compile($('#news_template').html())

    load_next_content = ->
        page_counter += 1

        Smooster.folder.id("53918c3781b59a4cc6000028")
        Smooster.folder.per_page(2)
        Smooster.folder.page(page_counter)
        Smooster.folder.pages().done (result) ->
          $.each result, (index, value) ->
            Smooster.page.get(value.id, Smooster.folder._id).done (result) ->
              data =
                id: value.id
                url: result.url
                date: Smooster.page.find_name_in_content_elements(result.contentElements, "text_h5").body
                picture: Smooster.page.find_name_in_content_elements(result.contentElements, "picture-blog1").body
                headline: Smooster.page.find_name_in_content_elements(result.contentElements, "text_h2").body
                teaser: Smooster.page.find_name_in_content_elements(result.contentElements, "text_a").body
              x = template.render(data)
              console.log(x)
              $('#news').append(x)


    $('#next').click (event) =>
      event.preventDefault()
      load_next_content()
