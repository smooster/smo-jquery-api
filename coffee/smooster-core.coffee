@Smooster ||= {}
jQuery.extend @Smooster,
  version: '0.1.2'

  debug: false

  api: new API()

  debugMsg: (msg) ->
    return unless @debug
    console.log(msg)

  folder: new Folder()

  page: new Page()
