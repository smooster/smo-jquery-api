class Folder
  _id: ""
  _per_page: 100
  _order_type: "desc"
  _without_index: "false"
  _descendants_and_self: "false"
  _page: 1

  id: (value) ->
    @_id = value

  per_page: (num) ->
    @_per_page = num

  order_type: (str) ->
    @_order_type = str

  without_index: (bool) ->
    @_without_index = bool

  descendants_and_self: (bool) ->
    @_descendants_and_self = bool

  page: (num) ->
    @_page = num

  media_assets: ->
    Smooster.debugMsg("not implemented yet")

  pages: ->
    unless @_id.length > 0
      Smooster.debugMsg("No folder id defined")
      return false
    options =
      endpoint: "/folders/#{@_id}/pages"
      query: {per_page: @_per_page, order_type: @_order_type, page: @_page, without_index: @_without_index, descendants_and_self: @_descendants_and_self}

    Smooster.api.ajax(options)
