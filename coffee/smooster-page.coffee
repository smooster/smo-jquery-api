class Page
  _id: ""

  get: (id, folder_id) ->
    @_id = id

    options =
      endpoint: "/folders/#{folder_id}/pages/#{@_id}"

    Smooster.api.ajax(options)

  find_name_in_content_elements: (haystack, needle) ->
    x = $.grep(haystack, (e) ->
                e.name is needle
              )
    return x[0]
