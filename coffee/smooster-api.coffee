class API
  _url: "http://cms.smooster.com/api"
  #_url: "http://127.0.0.1/api"

  ajax: (options=nil) ->
    $.ajax
        data: options.query
        type: "get"
        url: "#{@_url}#{options.endpoint}"
